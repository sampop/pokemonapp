const PokemonInfoModal = ({ pokemon, setModalVisible }) => {
  return (
    <div className="modal" onClick={() => setModalVisible(false)}>
      <div className="modal-content">
        <p className="modal-content__title">{pokemon.name}</p>
        <p>HP {pokemon.stats[0].base_stat}</p>
        <img
          className="modal-content__image"
          src={pokemon.sprites.other["official-artwork"].front_default}
          alt={pokemon.name}
        />
      </div>
    </div>
  );
};

export default PokemonInfoModal;
