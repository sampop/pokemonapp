const PokemonCard = ({
  pokemon,
  setModalVisible,
  setSelectedPokemon,
  getPokemons,
}) => {
  return (
    <div
      className="pokemon-card"
      onClick={() => {
        setSelectedPokemon(pokemon);
        setModalVisible(true);
        getPokemons();
      }}
    >
      <div className="pokemon-card__title">
        <p>{pokemon.name}</p>
      </div>
      <img
        className="pokemon-card__image"
        src={pokemon.sprites.other["official-artwork"].front_default}
        alt={pokemon.name}
      />
    </div>
  );
};

export default PokemonCard;
