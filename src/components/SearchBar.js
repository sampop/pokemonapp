const SearchBar = ({ searchPokemon }) => {
  const handleSubmit = (event) => {
    event.preventDefault();
    searchPokemon(event.target.input.value);
  };

  return (
    <div className="search-bar">
      <form onSubmit={handleSubmit}>
        <div>
          <input type="text" name="input" placeholder="search" />
          <button type="submit">🔍</button>
        </div>
      </form>
    </div>
  );
};

export default SearchBar;
