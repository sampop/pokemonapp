import axios from "axios";
import { useEffect, useState } from "react";
import "./App.css";
import PokemonCard from "./components/PokemonCard";
import PokemonInfoModal from "./components/PokemonInfoModal";
import SearchBar from "./components/SearchBar";

const App = () => {
  const [pokemons, setPokemons] = useState([]);
  const [selectedPokemon, setSelectedPokemon] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  const getPokemons = async () => {
    let fetchedPokemons = [];
    let randomNumbers = [];
    let pokemonCount;
    const pokemonAmount = 20;

    await axios
      .get("https://pokeapi.co/api/v2/pokemon-species/?limit")
      .then((response) => {
        pokemonCount = response.data.count;
      });

    while (randomNumbers.length < pokemonAmount) {
      let r = Math.floor(Math.random() * pokemonCount) + 1;
      if (randomNumbers.indexOf(r) === -1) randomNumbers.push(r);
    }

    for (let i = 0; i < pokemonAmount; i++) {
      await axios
        .get(`https://pokeapi.co/api/v2/pokemon/${randomNumbers[i]}`)
        .then((response) => {
          fetchedPokemons.push(response.data);
        });
    }
    setPokemons(fetchedPokemons);
  };

  const searchPokemon = async (searchQuery) => {
    await axios
      .get(`https://pokeapi.co/api/v2/pokemon/${searchQuery.toLowerCase()}`)
      .then((response) => {
        setSelectedPokemon(response.data);
        setModalVisible(true);
      })
      .catch(function (error) {
        console.log(error);
        alert("No pokémon found");
      });
  };

  useEffect(() => {
    getPokemons();
  }, []);

  return (
    <div className="main">
      <h1>20 Random Pokémon</h1>
      <SearchBar searchPokemon={searchPokemon} />
      <div className="pokemons">
        {pokemons.map((pokemon) => (
          <PokemonCard
            pokemon={pokemon}
            key={pokemon.id}
            setModalVisible={setModalVisible}
            setSelectedPokemon={setSelectedPokemon}
            getPokemons={getPokemons}
          />
        ))}
      </div>
      {modalVisible && (
        <PokemonInfoModal
          pokemon={selectedPokemon}
          setModalVisible={setModalVisible}
        />
      )}
    </div>
  );
};

export default App;
